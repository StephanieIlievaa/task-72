# A React Task
The task is to create a component for displaying a **list of tags**.
## Objective
Checkout the dev branch.
Create a **Tags** component and render it inside the App component.
When implemented, merge the dev branch into master.
## Requirements

The project starts with **npm run start**.

