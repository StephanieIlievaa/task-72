import React from 'react';

const Tags = (props) => {

  return (
    <div className='tags'>
      {
        props.tags.map(tag => <h1 className='tag'>#{tag}</h1>)
      }
    </div>
  )
}

export default Tags;

// export default class Tags extends Component {
//   render() {
//       const tagsList = this.props.arrayTags.map((item) =>(
//           <li class='tag'>#{item}</li>
//              ));

//       return (
//           <ul class="tags"> {tagsList} </ul>
//       );
//   }
// }

// Tags.defaultProps = {
//   arrayTags: ["boomdotdev", "task", "tags", "react"]
// };
